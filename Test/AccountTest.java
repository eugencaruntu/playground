import org.junit.Assert;
import org.junit.Test;

public class AccountTest {
    private double epsilon = 1e-6;

    @Test
    public void accountCannotHaveNegativeOverdraftLimit() {
        Account account = new Account(-20);

        Assert.assertEquals(0d, account.getOverdraftLimit(), epsilon);
    }

    @Test
    public void notAcceptingNegativeDeposit() {
        Account account = new Account(-20);

        Assert.assertFalse(account.deposit(-5));
    }

    @Test
    public void notAcceptingNegativeWithdrawals() {
        Account account = new Account(0);

        Assert.assertFalse(account.withdraw(-5));
    }

    @Test
    public void correctDeposit() {
        Account account = new Account(0);
        account.deposit(20);
        Assert.assertEquals(20, account.getBalance(), epsilon);
    }

    @Test
    public void accountWontOverstepOverdraftLimit() {
        Account account = new Account(0);
        Assert.assertFalse(account.withdraw(22));
    }

    @Test
    public void correcWithdrawal() {
        Account account = new Account(20);
        account.withdraw(5);
        Assert.assertEquals(-5, account.getBalance(), epsilon);
    }


    @Test
    public void correctDepositResult() {
        Account account = new Account(0);
        Assert.assertTrue(account.deposit(20));
    }

    @Test
    public void correcWithdrawalResult() {
        Account account = new Account(20);
        Assert.assertTrue(account.withdraw(5));
    }

}