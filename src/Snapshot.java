import java.util.ArrayList;

public class Snapshot {
    private final ArrayList<Integer> original;

    public Snapshot(ArrayList<Integer> data) {
        this.original = cloneList(data);
    }

    public ArrayList<Integer> restore() {
        return cloneList(this.original);
    }

    public ArrayList<Integer> cloneList(ArrayList<Integer> list) {
        ArrayList<Integer> clone = new ArrayList<Integer>(list.size());
        clone.addAll(list);
        return clone;
    }

    public static void main(String[] args) {
        ArrayList<Integer> list = new ArrayList<Integer>();
        list.add(1);
        list.add(2);
        Snapshot snap = new Snapshot(list);

        list.set(0, 3);
        list = snap.restore();
        System.out.println(list); //It should log "[1,2]"

        list.add(4);
        list = snap.restore();
        System.out.println(list); //It should log "[1,2]"
    }
}