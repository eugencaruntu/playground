public class Palindrome {
    public static boolean isPalindrome(String word) {
        if (word.length() == 0) {
            return false;
        }
        int s = 0;
        int e = word.length() - 1;

        while (s < e) {
            if (Character.toLowerCase(word.charAt(s)) != Character.toLowerCase(word.charAt(e))) {
                return false;
            }
            s++;
            e--;
        }
        return true;
    }

    public static void main(String[] args) {
        System.out.println(Palindrome.isPalindrome("Deleveled"));
    }
}