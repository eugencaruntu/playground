public class SortedSearch {
    private static int findMid(int[] sortedArray, int l, int r, int lessThan) {
        int c = l + (r - l) / 2;
        if (sortedArray[c] == lessThan) {
            // move to right as long as equals
            while (c - 1 > 0 && sortedArray[c - 1] == lessThan) {
                c--;

            }
            return c;
        }
        // move to left as long as smaller or equal
        if (c <= l) {
            while (c <= r && sortedArray[c] < lessThan) {
                c++;

            }
            return c;
        }
        //
        if (sortedArray[c] < lessThan) {
            l = c;  // ignore left half
        } else {
            r = c;  // ignore right half
        }
        return findMid(sortedArray, l, r, lessThan);
    }

    public static int countNumbers(int[] sortedArray, int lessThan) {
        return findMid(sortedArray, 0, sortedArray.length - 1, lessThan);
    }

    public static void main(String[] args) {
        System.out.println(SortedSearch.countNumbers(new int[]{1, 3, 5, 7}, 4));
    }
}