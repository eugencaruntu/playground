import java.util.Deque;
import java.util.LinkedList;

public class TrainComposition {
    private Deque<Integer> train = new LinkedList<>();

    public void attachWagonFromLeft(int wagonId) {
        train.push(wagonId);
    }

    public void attachWagonFromRight(int wagonId) {
        train.add(wagonId);
    }

    public int detachWagonFromLeft() {
        return train.removeFirst();
    }

    public int detachWagonFromRight() {
        return train.removeLast();
    }

    public static void main(String[] args) {
        TrainComposition tree = new TrainComposition();
        tree.attachWagonFromLeft(7);
        tree.attachWagonFromLeft(33);
        tree.attachWagonFromLeft(13);
        System.out.println(tree.detachWagonFromRight()); // 7
        System.out.println(tree.detachWagonFromLeft()); // 13
    }
}