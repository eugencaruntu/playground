import java.util.Arrays;

import static java.lang.Math.floor;
import static java.lang.Math.log;

public class InsertionSort {
    static int comparisonsCont = 0;
    static int localComp;

    static int[] input = {2, 6, 7, 8, 3, 9, 1, 4, 5, 0};


    public static void main(String[] args) {

        System.out.println("\nThe sorted array is" + Arrays.toString(insertionSort(input)));
        System.out.println("comparisons count: " + comparisonsCont);

    }

    public static int[] insertionSort(int[] a) {
        int n = a.length;

        for (int i = 1; i < n; i++) {
            int tmp = a[i];
            System.out.println("\nThe sorted array so far is " + Arrays.toString(a));
            System.out.println("we insert " + tmp + " into array of size " + i);

            localComp = 0;
            int p = binarySearch(a, 0, i, tmp);
            System.out.println("Local comps: " + localComp + " + 1 = " + (localComp + 1));

            for (int j = i - 1; j >= p; j--) {
                a[j + 1] = a[j];
            }
            a[p] = tmp;
        }

        return a;
    }


    public static int binarySearch(int arr[], int l, int r, int x) {

        System.out.println("Floor [ log " + (r - l) + " + 1 ] = " + floor(log(r - l) + 1) + "  [ " + l + " - " + r + " ]");

        int mid = l + (r - l) / 2;

        // one element only
        if (r == l) {
            return r;
        }

        // compare to middle and go right or left
        localComp++;
        comparisonsCont++;
        if (arr[mid] > x) {
            return binarySearch(arr, l, mid, x);
        } else if (arr[mid] < x) {    // look to left or equal
            return binarySearch(arr, mid + 1, r, x);
        } else {    // equal case
            return mid;
        }

    }
}