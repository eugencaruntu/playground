public class RemoveRepetitions {
    public static String transform(String input) {
        StringBuilder result = new StringBuilder();
        Character current = input.charAt(0);
        result.append(current);
        for (int i = 0; i < input.length(); i++) {
            if (input.charAt(i) != current) {
                result.append(input.charAt(i));
                current = input.charAt(i);
            }
        }

        return String.valueOf(result);
    }

    public static void main(String[] args) {
        System.out.println(RemoveRepetitions.transform("abbcbbb"));
    }
}