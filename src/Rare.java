import java.util.TreeMap;

public class Rare {

    public static int nthMostRare(int[] elements, int n) {
        if (elements.length == 1) {
            return elements[0];
        }
        TreeMap<Integer, Integer> map = new TreeMap<>();
        TreeMap<Integer, Integer> swapped = new TreeMap<>();
        for (int element : elements) {
            int count = map.getOrDefault(element, 0);
            map.put(element, count + 1);
            swapped.put(count + 1, element);
        }

        return swapped.get(n);

    }

    // https://en.wikipedia.org/wiki/Selection_algorithm
    public static int kth_selection(int[] elements, int n) {

        int minIdx = 0;
        int minVal = elements[minIdx];
        for (minIdx = 1; minIdx < elements.length; minIdx++) {
            if (elements[minIdx] < minVal) {

                minVal = elements[minIdx];
                elements[minIdx] = elements[minIdx - 1];
                elements[minIdx - 1] = minVal;
            }
        }

        return elements[n];
    }


    public static void main(String[] args) {
        int x = nthMostRare(new int[]{5, 4, 3, 2, 1, 5, 4, 3, 2, 5, 4, 3, 5, 4, 5}, 2);
        int y = kth_selection(new int[]{5, 4, 3, 2, 1, 5, 4, 3, 2, 5, 4, 3, 5, 4, 5}, 2);
        System.out.println(x);
        System.out.println(y);
    }
}